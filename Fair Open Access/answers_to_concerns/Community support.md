A [survey of 1000 mathematicians in 2016] (http://www.ems-ph.org/journals/show_pdf.php?issn=1027-488x&vol=3&iss=103&rank=9) asked, among other things, the following questions.  The rsults show very clearly that editors are strongly supported by the community to pursue converting their journals.

* What should the academics on the editorial boards of journals do if their efforts to reform are blocked by the publisher? (single answer allowed, percentages of receipients giving each answer  in parentheses)

1) Simply continue in their role at their present journal providing valuable services (4.3%)

2) Negotiate with the publisher to reform the journal itself (28.4%)

3) Resign and join or start a better run journal (31.7%)

4) Wait until there is a clear majority of editors agreeing on a plan, and leave to start another journal if the publisher will not accept the editors' demands (29.1%)

5) Other (6.5%)

* Please indicate  the importance of each factor to your opinion of journal reputation (1-5 scale, where 1 is lowest, 5 highest). 

1) Journal impact factor.

2) Open access status of journal.

3) External rankings (e.g. Australian Research Council ERA).

4) Historical reputation (``everyone knows Annals and Inventiones are top journals", etc).

5) Editorial board research quality.

6) Name of publishing company/organization.

7) Selectivity of journal.

8) Quality of peer review process.

The most important factor for respondents was the quality of peer review (median rank 5). This was folowed by the reputation of editors and historical reputation, and selectivity (median 4), then Journal Impact Factor (JIF), Open Access status and external rankings (median 3). The publisher had the lowest median ranking (2), with a mode of 1.



