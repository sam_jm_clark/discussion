Running a standard modern journal is fairly cheap. There are several good online publishing platforms with very low cost, including Scholastica, Mersenne and OJS. Costs per paper for basic services are typically around $10 or less. The main extra costs are copyediting/typesetting and editorial stipends, which can add up to a few hundreds of dollars per paper. Mersenne, for example, offers copyediting for 7 euros per page. 

A survey of 1000 mathematicians showed that 43% opposed editorial stipends under any circumstances.

Some editors are given teaching buyout often paid by the publisher, and some are given time of an editorial assistant.

For example, the board of Journal of Algebraic Combinatorics moved the journal with support from organizations including Foundation Compositio Mathematica, RNBM (network of French mathematics libraries) amounting to less than 10000 euros per year, much of which is unspent.