[PsychOpen] (http://www.psychopen.eu/) is an open-access publishing platform for psychology, operated by Leibniz Institute for Psychology Information (ZPID) [https://www.zpid.de/en], Trier, Germany. Currently, seven journals are published with PsychOpen. The PsychOpen platform is part of ZPID's  overall open science strategy in psychology (free search portal PubPsych, data archive PsychData, repository PsychArchives, etc).

Publishing service is free of charge (no APCs or any other fees).  PsychOpen mainly supports journals based in Europe or with a European focus.

Supported journals receive full publishing service, including:
* hosting journal management system (OJS) and technical support;
* editorial and user support;
* publication quality control, including anti-plagiarism checking (iThenticate), automated review of basic statistical results (Statcheck), reference list checking (eXtyles), APA style review;
* copyediting (eXtyles), converting MS Word submissions into high-quality XML (JATS) format, equations are transformed to MathML (LaTeX submission is not supported);
* page layout and production (Antenna House Formatter);
* DOI registration (Crossref);
* indexing (e.g., Scopus, PubMed Central, DOAJ);
* long-term archiving (CLOCKSS);
* content promotion support.

They do not support LaTeX submission. ORCID integration is in preparation (as of 2017) 

Articles are published under a CC-BY license, authors retain ownership of the copyright.

PsychOpen adheres to mandatory quality criteria (e.g., peer review, endorsement by learned society, international editorial board, etc.) so that the journals under its roof acquire a good reputation.

Example journals published with PsychOpen:

* [Journal of Numerical Cognition] (http://jnc.psychopen.eu/)
* [Journal of Social and Political Psychology] (http://jspp.psychopen.eu/)
* [Europe's Journal of Psychology] (http://ejop.psychopen.eu/)

