A company with much experience in typesetting and copyediting mathematical-heavy articles and books (they currently work for several mainstream mathematical journals). They do not publish journals from peer-review to production yet, but have experience in project management (e.g. the book Princeton Companion to Mathematics) and could extend their services, or can be used for the production part on top of another service provider.

Prices are highly customizable depending on the variety and level of service, the full service not exceeding a few hundred dollars/euros for a medium-sized paper.

Example journals typeset by T&T:

* Proceedings of the Royal Society of Edinburgh (Series A) 
* Journal de l’Institut Mathématique de Jussieu
